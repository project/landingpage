<?php

/**
 * @file
 * Installation hooks for landingpage_event_example module.
 */

use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Implements hook_install().
 */
function landingpage_event_example_install() {
  global $base_url;

  // Install additional theme for demo.
  \Drupal::service('theme_installer')->install(['landingpage_event']);

  // Disable blocks in landingpage_cv theme.
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_account_menu')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_branding')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_breadcrumbs')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_footer')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_help')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_local_actions')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_local_tasks')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_main_menu')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_messages')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_page_title')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_powered')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_search')->delete();
  \Drupal::configFactory()->getEditable('block.block.landingpage_event_tools')->delete();

  $node = _landingpage_import_yaml('landingpage_event_example', 'landingpage_event_example');
  // Set alias.
  \Drupal::service('path_alias.repository')->save("/node/" . $node->id(), "/landingpage/examples/event");

  \Drupal::messenger()->addMessage(t('You can find LandingPage Event Example on <a href="@base/landingpage/examples/event">this page</a>.', array('@base' => $base_url)), 'status');
}

/**
 * Implements hook_uninstall().
 */
function landingpage_event_example_uninstall() {

  $path = \Drupal::service('path_alias.manager')->getPathByAlias("/landingpage/examples/event");
  if ($path != "/landingpage/examples/event") {
    $path_parts = explode("/", $path);
    if (is_numeric($path_parts[2])) {
      $node = Node::load($path_parts[2]);
      if (!empty($node)) {
        $node->delete();
        \Drupal::messenger()->addMessage(t('An example of LandingPage Event was deleted.'), 'status');
      }
    }
  }
}
